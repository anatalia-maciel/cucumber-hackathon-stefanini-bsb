package stepsDefinitions;

import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;
import pageObjects.BasePage;

public class PesquisarDiretorioStep {
	
	BasePage BasePage = new BasePage();
	
	@Quando("acionar a aba Directory")
	public void acionarAAbaDirectory() {
	    BasePage.clicarAbaDirectory();
	    
	}

	@Entao("o sistema apresenta o diretorio {string}")
	public void oSistemaApresentaODiretorio(String string) {
	   
	}

}