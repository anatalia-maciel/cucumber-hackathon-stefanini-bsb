package stepsDefinitions;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;
import pageObjects.BasePage;
import pageObjects.ManterUsuarioPage;
import pageObjects.TelaUsuariosPage;

public class ManterCadastroStep {
	
	BasePage BasePage = new BasePage();
	TelaUsuariosPage tp = new TelaUsuariosPage();
	ManterUsuarioPage mu = new ManterUsuarioPage();
	
	//Adicionar no final do nome HORA_MINUTO_SEGUNDO - para mudar dinamicamente. Nao aceita usuario com o mesmo nome
	String addComplementoNome = new SimpleDateFormat("HH_mm_ss").format(Calendar.getInstance().getTime());
	
	
	
	@Quando("acionar a aba Admin")
	public void acionarAAbaAdmin() {
	    BasePage.clicarAbaAdmin();
	}

	@Quando("clicar no usuario {string}")
	public void clicarNoUsuario(String nomeUsuario) {
	    tp.clicarNoUsuario(nomeUsuario);
	}

	@E("^acionar o botao EditSalvar$")
	public void acionarOBotaoEditSalvar() throws Throwable {
		Thread.sleep(2000);
		mu.clicarBotaoEditar();
	}

	@Quando("informar no campo username {string}")
	public void informarNoCampoUsername(String nomeUsuario) {
	    mu.escreverNoCampoUsername(nomeUsuario + addComplementoNome);
	}

	@Entao("o sistema cadastra o usuario {string}")
	public void oSistemaCadastraOUsuario(String nomeUsuario) throws Throwable {
		tp.validarUsuarioCadastrado(nomeUsuario + addComplementoNome);
	}


}
