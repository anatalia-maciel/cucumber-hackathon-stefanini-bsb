package stepsDefinitions;

import io.cucumber.java.it.Quando;
import io.cucumber.java.pt.Entao;
import pageObjects.BasePage;

public class AcessarDashboardStep {
	
	BasePage BasePage = new BasePage();
	
	@Quando("acionar a aba Dashboard")
	public void acionarAAbaDashboard() {
		BasePage.clickabaDashboard();
			    
	}
	@Entao("o sistema apresenta a legenda")
	public void oSistemaApresentaALegenda() {
	  
	}

}