package pageObjects;

import static utils.Utils.driver;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ManterUsuarioPage {
	
	public ManterUsuarioPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(name = "btnSave")
	private WebElement botaoEditar_Salvar;
	
	@FindBy(id = "systemUser_userName")
	private WebElement campoUsername;
	
	public void clicarBotaoEditar() {
		botaoEditar_Salvar.click();
	}
	
	public void escreverNoCampoUsername(String nomeUsuario) {
		campoUsername.clear();
		esperar();
		campoUsername.sendKeys(nomeUsuario);
	}

	private void esperar() {
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			
			e.printStackTrace();
		}
	}
	
}
