package pageObjects;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static utils.Utils.driver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class BasePage {
	
	

	public BasePage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id = "welcome")
	private WebElement textoUsuarioLogado;
	
	@FindBy(xpath = "//span[@id='notification']/.")
	private WebElement iconeSino;
	
	@FindBy(id = "menu_admin_viewAdminModule")
	private WebElement abaAdmin;
	
	@FindBy(id = "menu_directory_viewDirectory")
	private WebElement abaDirectory;
	
	@FindBy(id = "menu_dashboard_index")
	private  WebElement abaDashboard;

	@FindBy(id = "menu_time_viewTimeModule")
	private WebElement abaTime;
	
	@FindBy(id = "menu_pim_viewPimModule")
	private WebElement abaPIM;
	
	
	
	public void validarUsuarioLogado(String string) {
		assertEquals(string, textoUsuarioLogado.getText());
	}
	
	public void validarIconeSino() {
		assertFalse(iconeSino.isSelected());
		System.out.println("Esse e o retorno: " + iconeSino.isDisplayed());
	}
	
	public void clicarAbaAdmin() {
		abaAdmin.click();
	}

	public void clicarAbaDirectory() {
		abaDirectory.click();
		
	}

	public void clickabaDashboard() {
		abaDashboard.click();
		
	}

	public void clickabaTime() {
		abaTime.click();
		
	}

	public void clickabaPIM() {
		abaPIM.click();
		
	}
	

}
