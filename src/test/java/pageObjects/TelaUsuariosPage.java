package pageObjects;

import static org.junit.Assert.assertTrue;
import static utils.Utils.driver;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class TelaUsuariosPage {
	
	//Construtor
	public TelaUsuariosPage() {
		PageFactory.initElements(driver, this);
	}
	
	//ELMENTOS
//	@FindBy(xpath = "//tr//td//a")
//	private List<WebElement> nomesUsuarios;
	
	//METODOS
	public void clicarNoUsuario(String nomeUsuario) {
		//WebElement usuario = driver.findElement(By.xpath("//a[.='" + nomeUsuario + "']"));
		WebElement	usuario = driver.findElement(By.xpath("//*[@id=\"resultTable\"]/tbody/tr[2]/td[2]/a"));
		usuario.click();
//		driver.findElement(By.xpath("//a[.='" + nomeUsuario + "']")).click(); tambem e possivel fazer assim
	}
	
	@FindBy(xpath = "//tr//td//a")
	private List<WebElement> nomesUsuarios;
	
	
	public void validarUsuarioCadastrado(String nomeUsuario) {
				
		assertTrue(driver.findElement(By.xpath("//a[.='" + nomeUsuario + "']")).isDisplayed());

			boolean existeUsuario = Boolean.FALSE;
			for (WebElement usuarioLista : nomesUsuarios) {
				if(usuarioLista.getText().equals(nomeUsuario)) {
					existeUsuario = Boolean.TRUE;
					break;
				}	
							
			}
			
			assertTrue(existeUsuario);
			

	}
	
	

}
