package utils;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Utils {
	
	public static ChromeDriver driver;
	//public static FirefoxDriver driver;
	
	public static void acessarSistema() {
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.home")+"/develop/webdriver/84/chromedriver");
		//System.setProperty("webdriver.gecko.driver", "/home/admin/develop/webdriver/geckodriver");
		
		driver = new ChromeDriver();
		//driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
		driver.get("https://opensource-demo.orangehrmlive.com/");
	}

}
